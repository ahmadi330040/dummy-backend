const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const moment = require('moment')
const promMid = require('express-prometheus-middleware');

let port = 8089

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))
app.use(promMid({
  metricsPath: '/metrics',
  collectDefaultMetrics: true,
  requestDurationBuckets: [0.1, 0.5, 1, 1.5],
  requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
  responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
}))

app.get('/', (req, res) => {
  console.log(`[REQUEST][get][${moment().utc().add(7, 'hours').format('DD/MM/YYYY HH:mm:ss')}] :`, req.headers)
  return res.json({
    status: 200,
    message: 'Service is running !'
  })
})

app.use((req, res) => {
  console.log(`[REQUEST][use][${moment().utc().add(7, 'hours').format('DD/MM/YYYY HH:mm:ss')}] :`, req.headers)
  return res.json({
    status: 200,
    message: 'Service is running !'
  })
})

app.listen(port, '::', () => console.log('[!] SERVICES RUNNING ON PORT : '+port))